# Readme
The first thing to read when joining the group

## Licensing
- Place a `LICENSE` in the root of each repo
- Use the one [here](LICENSE) as a starting point

## Editor Config
- Place an `.editorconfig` in the root of each repo
- Use the one [here](.editorconfig) as a starting point